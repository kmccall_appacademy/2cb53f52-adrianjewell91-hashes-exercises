# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  counter = Hash.new(0)
  str.split.each {|el| counter[el] = el.length}
  counter
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  hash.sort_by{|k,v| v}[-1][0]
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each {|k,v| older[k] = v}
  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  counter = Hash.new(0)
  word.each_char {|ch| counter[ch] += 1}
  counter
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  counter = Hash.new(0)
  arr.each{|el| counter[el] =true }
  counter.keys
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hash_parities = {:even => 0, :odd => 0}

  numbers.each do |num|
    unless num.odd?
      hash_parities[:even] += 1
    else
      hash_parities[:odd] += 1
    end
  end
  hash_parities

end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  #do a hash with vowels and count them, then return the highest.
  counter = Hash.new(0)
  string.each_char do |el|
    counter[el] += 1 unless "aeiou".include?(el) == false
  end

  #return key with max value
  #Select all max values and then return the first one.
  max_values = counter.select{|k,v| v == counter.values.max}
  max_values.sort_by{|k,v| k}[0][0] #returns the first one in the alphabet.
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# [bertie, dottie, warren], [bertie, dottie,], [bertie, warren] [dottie, warren]
# For each element, push [el, next_el] with next_el changing until next_el == nil

# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]

def fall_and_winter_birthdays(students)
  second_half_kids = students.select{|k,v| v>6}.keys
  arr = []

  #What do I do? For each el, push all the songs after it with the next song.
  second_half_kids.each_with_index do |stud, idx|

      remaining_students = second_half_kids[(idx+1)..-1]

      remaining_students.each do |el|
        arr << [stud, el]
      end

  end
  arr
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  counter = Hash.new(0)
  specimens.each {|el| counter[el] += 1}

  number_of_species = counter.length
  smallest_population_size = counter.values.min
  largest_population_size =  counter.values.max

  (number_of_species**2) * (smallest_population_size/largest_population_size)
  #Three variables
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
#WHAT DO I DO?

def can_tweak_sign?(normal_sign, vandalized_sign)

  normal = character_count(normal_sign)
  vandalized = character_count(vandalized_sign)

  vandalized.each_key {|key| return false if vandalized[key] > normal[key]}

  true
end

def character_count(str)
  str = str.delete(" .,!?'")
  counter = Hash.new(0)
  str.each_char{|ch| counter[ch.downcase] += 1}
  counter
end
